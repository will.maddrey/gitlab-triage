# frozen_string_literal: true

require_relative 'members_conditions_filter'

module Gitlab
  module Triage
    module Filters
      class AssigneesMemberConditionsFilter < MembersConditionsFilter
        def member_field
          :assignees
        end
      end
    end
  end
end
