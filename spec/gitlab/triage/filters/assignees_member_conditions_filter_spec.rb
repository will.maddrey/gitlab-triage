# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/filters/assignees_member_conditions_filter'

describe Gitlab::Triage::Filters::AssigneesMemberConditionsFilter do
  let(:type) { :assignees }

  it_behaves_like 'a members filter'
end
