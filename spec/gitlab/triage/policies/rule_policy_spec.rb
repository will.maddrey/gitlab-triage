# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/policies/rule_policy'
require 'gitlab/triage/policies_resources/rule_resources'

describe Gitlab::Triage::Policies::RulePolicy do
  include_context 'with network context'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { item: '' }, issue: { title: '' } } }
  let(:policy_spec) { { name: name, actions: actions } }

  describe '#build_summary' do
    let(:resources) { Gitlab::Triage::PoliciesResources::RuleResources.new([]) }

    subject { described_class.new(type, policy_spec, resources, network) }

    it 'delegates to EntityBuilders::SummaryBuilder' do
      expect(Gitlab::Triage::EntityBuilders::SummaryBuilder)
        .to receive(:new)
        .with(
          type: type,
          policy_spec: policy_spec,
          action: actions[:summarize],
          resources: resources,
          network: network)
        .and_call_original

      subject.build_summary
    end
  end

  describe '#build_issue' do
    let(:resource) { { name: name } }

    subject { described_class.new(type, policy_spec, Gitlab::Triage::PoliciesResources::RuleResources.new([]), network) }

    it 'delegates to EntityBuilders::IssueBuilder' do
      expect(Gitlab::Triage::EntityBuilders::IssueBuilder)
        .to receive(:new)
        .with(
          type: type,
          policy_spec: policy_spec,
          action: actions[:issue],
          resource: resource,
          network: network)
        .and_call_original

      subject.build_issue(resource)
    end
  end
end
