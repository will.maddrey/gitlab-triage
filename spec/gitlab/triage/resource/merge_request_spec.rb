# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/merge_request'

describe Gitlab::Triage::Resource::MergeRequest do
  include_context 'with network context'

  it_behaves_like 'issuable'

  describe '#first_contribution?' do
    include_context 'with stubs context'

    subject { described_class.new(mr.with_indifferent_access, network: network) }

    context 'when first_contribution is not loaded' do
      it 'returns the value from the API' do
        stub_get = stub_api(:get, subject.__send__(:resource_url)) do
          { first_contribution: true }
        end

        expect(subject.first_contribution?).to be(true)

        assert_requested(stub_get)
      end
    end

    context 'when first_contribution is loaded' do
      before do
        mr[:first_contribution] = false
      end

      it 'returns the value directly' do
        expect(subject.first_contribution?).to be(false)
      end
    end
  end

  describe '#closes_issues' do
    include_context 'with stubs context'

    subject { described_class.new(resource, network: network) }

    let(:resource) { { project_id: 123, iid: 1 } }

    context 'when querying loads closes_issues from API' do
      it 'returns the value from the API' do
        stub_get = stub_api(
          :get,
          "#{base_url}/projects/123/merge_requests/1/closes_issues",
          query: { per_page: 100 }
        ) do
          [{ iid: 321 }]
        end

        closes_issues = subject.closes_issues
        expect(closes_issues.size).to eq(1)
        expect(closes_issues[0].resource[:iid]).to eq(321)
        assert_requested(stub_get)
      end
    end
  end

  describe '#url' do
    subject { described_class.new(resource, network: network).__send__(:url) }

    let(:resource) { { source_id_name => 123 } }

    context 'when source is project' do
      let(:source_id_name) { :project_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq(
          "#{base_url}/projects/123/merge_requests?per_page=100")
      end
    end

    context 'when source is group' do
      let(:source_id_name) { :group_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq(
          "#{base_url}/groups/123/merge_requests?per_page=100")
      end
    end
  end
end
