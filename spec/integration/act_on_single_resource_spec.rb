# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'act on a single resource' do
  include_context 'with integration context'

  let(:rule) do
    <<~YAML
    resource_rules:
      issues:
        rules:
          - name: Rule name
            conditions:
              state: opened
            actions:
              comment: |
                This is my comment for \#{resource[:type]}
      merge_requests:
        rules:
          - name: Rule name
            conditions:
              state: opened
            actions:
              comment: |
                This is my comment for \#{resource[:type]}
      epics:
        rules:
          - name: Rule name
            conditions:
              state: opened
            actions:
              comment: |
                This is my comment for \#{resource[:type]}
    YAML
  end

  describe 'act on a single issue' do
    let(:argv) { %W[--source #{source} --source-id #{source_id} --resource-reference #42 --token #{token}] }

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, state: 'opened', iids: '42' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        issues
      end
    end

    it 'comments on the issue' do
      stub_post = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
        body: { body: 'This is my comment for issues' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post)
    end
  end

  describe 'act on a single merge request' do
    let(:argv) { %W[--source #{source} --source-id #{source_id} --resource-reference !#{merge_request[:iid]} --token #{token}] }

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, state: 'opened', iids: merge_request[:iid] },
        headers: { 'PRIVATE-TOKEN' => token }) do
        mrs
      end
    end

    it 'comments on the merge request' do
      stub_post = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{merge_request[:iid]}/notes",
        body: { body: 'This is my comment for merge_requests' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post)
    end
  end

  describe 'act on a single epic' do
    let(:source) { 'groups' }
    let(:source_id) { group_id }
    let(:argv) { %W[--source #{source} --source-id #{source_id} --resource-reference &#{epic[:iid]} --token #{token}] }

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/groups/#{group_id}/epics/#{epic[:iid]}",
        query: { per_page: 100, state: 'opened', iids: epic[:iid] },
        headers: { 'PRIVATE-TOKEN' => token }) do
        epics
      end
    end

    it 'comments on the epic' do
      stub_post = stub_api(
        :post,
        "https://gitlab.com/api/v4/groups/#{group_id}/epics/#{epic[:id]}/notes",
        body: { body: 'This is my comment for epics' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post)
    end
  end
end
