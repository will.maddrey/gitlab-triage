# frozen_string_literal: true

require 'gitlab/triage/options'
require 'gitlab/triage/network'
require 'gitlab/triage/network_adapters/httparty_adapter'

RSpec.shared_context 'with network context' do
  let(:network_options) do
    options = Gitlab::Triage::Options.new
    options.host_url = 'http://test.com'
    options.token = 'token'
    options.source_id = '123'
    options
  end

  let(:network) do
    Gitlab::Triage::RestAPINetwork.new(
      Gitlab::Triage::NetworkAdapters::HttpartyAdapter.new(network_options))
  end

  let(:base_url) do
    "#{network.options.host_url}/api/#{network.options.api_version}"
  end

  before do
    allow(network).to receive(:print)
  end
end
