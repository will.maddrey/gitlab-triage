# frozen_string_literal: true

RSpec.shared_context 'with user stubs context' do
  let(:admin) do
    {
      state: 'active',
      id: 1,
      web_url: 'https://gitlab.com/root',
      name: 'Administrator',
      avatar_url: nil,
      username: 'root'
    }
  end
end
